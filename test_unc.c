#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <assert.h>
#include <errno.h>
#include <sched.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define FILE_NAME "/dev/mem"
#define MEM_BASE 0x800000000
#define NTHREADS 4
#define CACHE_LINE 64
#define WORKING_SET 4 * 1024 //4KB
#define ITERATIONS 1000000

enum types {UNCACHED,L1,UNLOCKED};
pthread_barrier_t barr;
pthread_t threads[NTHREADS];
int l1_value,uncached_value,unlocked_value;

struct thread_args{
	void *l1_lock;
	void *uncached_lock;
};

int thread_id (void)
{   int i;
    pthread_t self = pthread_self();
    for ( i = 0; i < NTHREADS; i++) {
        if (threads[i] == self)
            return i;
    }
    return -1;
}

void sync_threads() {
    int res;
    res = pthread_barrier_wait(&barr);
    if (res == PTHREAD_BARRIER_SERIAL_THREAD || res == 0) {
        //printf("Thread %d reached point barrier\n", thread_id());
    } else {
        fprintf(stderr,"Error - pthread_barrier_wait return code: %d\n", res);
        exit(EXIT_FAILURE);
    }
}

void set_affinity (int cpu)
{
	//printf("%d thread in set aff\n",thread_id());
	cpu_set_t cpuset;
	int s;
	
    CPU_ZERO(&cpuset);
    CPU_SET(cpu, &cpuset);
    s = pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
    //printf("s is %d for thread %d\n",s,thread_id());
    if (s != 0) {
        perror("pthread_setaffinity_np\n");
        exit(EXIT_FAILURE);
    }
}   

void *threadfunction (void* arg)
{	
	struct thread_args *arguments = (struct thread_args *) arg;
	int *l1_lock = (int *) arguments->l1_lock;
    	int *uncached_lock = (int *) arguments->uncached_lock;
	//printf("mmap address of l1_lock:%d, mmap address for uncached_lock:%d INSIDE THREAD FUNCTION\n",l1_lock,uncached_lock);
	int threadid=thread_id();
	int size=sizeof(cpu_set_t);
	int cpu=threadid%size;
	set_affinity(cpu);
	//printf("affinity is set for thread %d\n",thread_id());
	sync_threads();
	while(__sync_lock_test_and_set (uncached_lock, 1)); 
	uncached_value++;
        __sync_lock_release (uncached_lock);
	
	sync_threads();
	while(__sync_lock_test_and_set (l1_lock, 1));
	l1_value++;
	__sync_lock_release (l1_lock);
	
	sync_threads();
	unlocked_value++;	
	//printf("address of l1_value: %d, address of uncached_value: %d, address of unlockedvalue: %d IN THREAD FUNCTION\n",&l1_value, &uncached_value,&unlocked_value);
}

int main(void) {
	
	struct thread_args thread_args;
	void *l1_lock, *uncached_lock, *arg;
	int k, i,ret,j,memfd;
	int fails[3]={0,0,0};	
	unsigned long offset = MEM_BASE;
	pthread_barrier_init(&barr, NULL, NTHREADS);
	
	memfd = open(FILE_NAME, O_RDWR | O_SYNC);
	if (memfd == -1) {
        	printf("Can't open %s\n", FILE_NAME);
        	exit(0);
    	}
	
	//printf("address of l1_value: %d, address of uncached_value: %d, address of unlockedvalue: %d\n",&l1_value, &uncached_value,&unlocked_value);
	
	l1_lock = mmap(0, WORKING_SET, PROT_READ | PROT_WRITE, MAP_SHARED| 0x40, memfd, offset);
	offset+=WORKING_SET;
	*(int *) l1_lock = 0;
	thread_args.l1_lock = l1_lock;
	
	uncached_lock = mmap(0, WORKING_SET, PROT_READ | PROT_WRITE, MAP_SHARED| 0x80, memfd, offset);
	*(int *) uncached_lock=0;
	thread_args.uncached_lock = uncached_lock;
	
	//printf("mmap address of l1_lock:%d, mmap address for uncached_lock:%d\n",l1_lock,uncached_lock);
	for(j=0;j<ITERATIONS;j++) {
		l1_value=0;uncached_value=0;unlocked_value=0;//printf("j=%d\n",j);
		for(i=0; i<NTHREADS;i++) {
			ret = pthread_create(&(threads[i]), NULL, &threadfunction,(void *) &thread_args);
			if(ret) {
		    		fprintf(stderr,"Error - pthread_create return code: %d\n", ret);
		    		exit(EXIT_FAILURE);
			}
			//printf("pthread_create() for thread %d returns: %d\n", i, ret);
		}
		for (i = 0; i < NTHREADS; i++) {
        		pthread_join(threads[i], NULL);
   		}	
		
		if(uncached_value!=NTHREADS) fails[UNCACHED]++;
		if(l1_value!=NTHREADS) fails[L1]++;
		if(unlocked_value!=NTHREADS) fails[UNLOCKED]++; 
	}

  	printf("uncached lock:%d fails out of %d\nL1 lock: %d fails out of %d\nunlocked: %d fails out of %d\n",fails[UNCACHED],j,fails[L1],j,fails[UNLOCKED],j);
  	return 1;
	
		
}








